// YouTube TS Course

// Console Output
console.log('Hello World!')

// const is constant
const a: string = '42'
const b: number = 42
const c: boolean = true
const d: string[] = ['Hello', 'World']
console.log(a, b, c, typeof a, typeof b, typeof c, typeof d)

// let is scoped
let hello: string = 'world'
hello = 'foo'
console.log(hello)

// Any type
let whatEver: any = 'world'
whatEver = 42
console.log(whatEver, typeof whatEver)

// Union Type
let unison: string | number
unison = 'Hello'
unison = 42
console.log(unison)

// Function
// Arrow functions is anonymous. The function keyword can be dropped
const getFullName = (name: string, surname: string): string => {
  return name + ' ' + surname
}
// Standard function declaration
function getName(name: string, surname: string): string {
  return name + ' ' + surname
}
console.log(getFullName('Frank', 'Drebin'), getName('Det.', 'Nordberg'))

// Objects
// Standard Object without interface
const use: { name: string; age: number } = {
  name: 'Frank',
  age: 42,
}
// Interface to be used in other objects. ? declaration is for optional. This keyword refers to the current object
interface UserInterface {
  name: string
  age?: number
  getMessage(): string
}
const user: UserInterface = {
  name: 'Jack',
  getMessage() {
    return 'Hello' + ' ' + this.name
  },
}
console.log(user.name, user.getMessage())

// error set default to null or it will be undefined
interface UseInterface {
  name: string
  surname: string
}
let userMan: UseInterface | null = null
let errorMessage: string | null = null

// Own Types
type ID = string
type Tag = string[]
type MaybePopTag = Tag | null

interface Types {
  id: ID
  tag: Tag
}
const popTag: Types = {
  id: 'test',
  tag: ['dragon', 'coffee'],
}

// Datatype
// Void when nothing gets returned. Void is undefined and null
const doSomething = (): void => {
  console.log('Void log')
}
// Any no error detection. Avoid this type
const foo: any = 'foo'
// Never? throw is an exeption and terminates the program
const doNever = (): never => {
  throw 'never'
}
// Unknown
const vUnknown: unknown = 10
// Type assertion
let s1: string = vUnknown as string
let pageNumber: string = '1'
let numPageNumber: number = pageNumber as unknown as number

// DOM script in head tag with defer or before body close
const someElement = document.querySelector('input') as HTMLInputElement
console.log(someElement.value)
let colElement = document.querySelector('.para') as HTMLParagraphElement
colElement.style.backgroundColor = 'lightblue'

// someElement.addEventListener('blur', (event) => {
//     const target = event.target as HTMLInputElement
//     console.log('event', target.value)
// })
//
// Classes private, public, protected. Public is default
interface classInterface {
  getFullName(): string
}
class UserClass implements classInterface {
  private firstName: string
  private lastName: string
  static readonly maxAge: number = 50

  constructor(firstName: string, lastName: string) {
    this.firstName = firstName
    this.lastName = lastName
  }

  getFullName(): string {
    return this.firstName + ' ' + this.lastName
  }
}
class Admin extends UserClass {
  private editor: string

  setEditor(editor: string): void {
    this.editor = editor
  }
  getEditor(): string {
    return this.editor
  }
}

const classuser = new UserClass('Frank', 'Drebin')
console.log(classuser)
console.log(classuser.getFullName())
console.log(UserClass.maxAge)

const admin = new Admin('Foo', 'Bar')
console.log(admin.getFullName())
admin.setEditor('TS')
console.log(admin.getEditor())
