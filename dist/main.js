// YouTube TS Course
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// Console Output
console.log('Hello World!');
// const is constant
var a = '42';
var b = 42;
var c = true;
var d = ['Hello', 'World'];
console.log(a, b, c, typeof a, typeof b, typeof c, typeof d);
// let is scoped
var hello = 'world';
hello = 'foo';
console.log(hello);
// Any type
var whatEver = 'world';
whatEver = 42;
console.log(whatEver, typeof whatEver);
// Union Type
var unison;
unison = 'Hello';
unison = 42;
console.log(unison);
// Function
// Arrow functions is anonymous. The function keyword can be dropped
var getFullName = function (name, surname) {
    return name + ' ' + surname;
};
// Standard function declaration
function getName(name, surname) {
    return name + ' ' + surname;
}
console.log(getFullName('Frank', 'Drebin'), getName('Det.', 'Nordberg'));
// Objects
// Standard Object without interface
var use = {
    name: 'Frank',
    age: 42
};
var user = {
    name: 'Jack',
    getMessage: function () {
        return 'Hello' + ' ' + this.name;
    }
};
console.log(user.name, user.getMessage());
var userMan = null;
var errorMessage = null;
var popTag = {
    id: 'test',
    tag: ['dragon', 'coffee']
};
// Datatype
// Void when nothing gets returned. Void is undefined and null
var doSomething = function () {
    console.log('Void log');
};
// Any no error detection. Avoid this type
var foo = 'foo';
// Never? throw is an exeption and terminates the program
var doNever = function () {
    throw 'never';
};
// Unknown
var vUnknown = 10;
// Type assertion
var s1 = vUnknown;
var pageNumber = '1';
var numPageNumber = pageNumber;
// DOM script in head tag with defer or before body close
var someElement = document.querySelector('input');
console.log(someElement.value);
var colElement = document.querySelector('.para');
colElement.style.backgroundColor = 'lightblue';
var UserClass = /** @class */ (function () {
    function UserClass(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    UserClass.prototype.getFullName = function () {
        return this.firstName + ' ' + this.lastName;
    };
    UserClass.maxAge = 50;
    return UserClass;
}());
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Admin.prototype.setEditor = function (editor) {
        this.editor = editor;
    };
    Admin.prototype.getEditor = function () {
        return this.editor;
    };
    return Admin;
}(UserClass));
var classuser = new UserClass('Frank', 'Drebin');
console.log(classuser);
console.log(classuser.getFullName());
console.log(UserClass.maxAge);
var admin = new Admin('Foo', 'Bar');
console.log(admin.getFullName());
admin.setEditor('TS');
console.log(admin.getEditor());
